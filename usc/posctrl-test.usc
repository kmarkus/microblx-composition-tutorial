-- Test overlay for the posctrl composition which exports a number of
-- ports via mqueues

return bd.system {

   imports = {
      "stdtypes",
      "mqueue",
      "ramp_double",
      "math_double",
   },

   subsystems = {
      bd.load("posctrl.usc"),
   },

   blocks = {
      { name="ramp", type="ubx/ramp_double" },
      { name="sin", type="ubx/math_double" },
   },

   configurations = {
      {
	 name="ramp",
	 config = {
	    slope={0.1, 0.1, 0.1, 0.1, 0.1, 0.1 },
	    data_len = "&num_joints",
	 }
      },
      {
	 name="sin",
	 config = {
	    func = "sin",
	    data_len = "&num_joints",
	 }
      },
      {
	 name = "trig",
	 config = {
	    tstats_mode = 2,
	    chain0 = {
	       { b = "#ramp" },
	       { b = "#sin" },
	       { b = "#arm" },
	       { b = "#velctrl" },
	       { b = "#posctrl" },
	    }
	 },
      },

   },

   connections = {
      { src="ramp.out", tgt="sin.x" },
      { src="sin.y", tgt="posctrl.des" },

      -- exported for testing / debugging
      { src="sin.y", type="ubx/mqueue" },
      { src="posctrl.out", type="ubx/mqueue" },
      { src="velctrl.out", type="ubx/mqueue" },
      { src="arm.jnt_pos_msr", type="ubx/mqueue" },
   }
}
