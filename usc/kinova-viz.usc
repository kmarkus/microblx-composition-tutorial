-- overlay the posctrl composition for use with the kinova arm

local MESH_BASE=os.getenv("SRC_PATH")..
   "ros_kortex/kortex_description/arms/gen3/7dof/meshes"

local MESH_NAMES = {
   "base_link.STL",
   "shoulder_link.STL",
   "half_arm_1_link.STL",
   "half_arm_2_link.STL",
   "forearm_link.STL",
   "spherical_wrist_1_link.STL",
   "spherical_wrist_2_link.STL",
   "bracelet_with_vision_link.STL"
}

return bd.system {

   imports = {
      "stdtypes", "lfds_cyclic", "mqueue",
      "iconst", "ramp_double", "math_double",
      "sim2b_bullet", "vis2b", "kinova_fpk",
   },

   subsystems = {
      bd.load("kinova-arm.usc"),
   },

   blocks = {
      { name = "fpk", type = "kinova_fpk" },
      { name = "vis", type = "vis2b/osg" },
   },

   configurations = {
      {
	 name="vis",
	 config = {
	    nr_joints = "&num_joints",
	    mesh_base = MESH_BASE,
	    mesh_names = table.concat(MESH_NAMES, ':') }
      },
      {
	 name = "trig",
	 config = {
	    chain0 = {
	       { b = "#posctrl" },
	       { b = "#velctrl" },
	       { b = "#arm" },
	       { b = "#fpk", every=10 },
	       { b = "#vis", every=10 }
	    }
	 },
      }
   },

   connections = {
      { src="arm.jnt_pos_msr", tgt="fpk.jnt_pos", config={loglevel_overruns=-1} },
      { src="fpk.poses", tgt="vis.poses" },
   }
}
