Tutorial: Composing a Cascaded Controller Application in microblx
=================================================================

Introduction
------------

This tutorial gives a hands-on introduction to the new composition
extensions introduced in microblx v0.8 and extended in v0.9.
Furthermore, the demo serves as a reference architecture for the
composition extensions.


<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Introduction](#introduction)
    - [Background](#background)
    - [Installation](#installation)
- [The Composable Cascaded Controller Application](#the-composable-cascaded-controller-application)
    - [velctrl.usc: velocity control](#velctrlusc-velocity-control)
    - [posctrl.usc: the outer position controller](#posctrlusc-the-outer-position-controller)
    - [posctrl-test.usc: the test bench](#posctrl-testusc-the-test-bench)
    - [kinova-arm.usc: switching to to a "real" robot](#kinova-armusc-switching-to-to-a-real-robot)
    - [kinova-viz.usc: adding visualization](#kinova-vizusc-adding-visualization)
    - [Summary](#summary)
- [Limitations](#limitations)
- [Related work](#related-work)
- [Acknowledgement](#acknowledgement)

<!-- markdown-toc end -->


### Background

microblx supports so called `usc` (microblx system composition)
models, that permit describing microblx applications using a
declarative, textual domain specific language. If you are not familiar
with the basic language, it may be helpful to skim through the section
[composing microblx
systems](https://microblx.readthedocs.io/en/latest/composing_systems.html#microblx-system-composition-dsl-usc-files)
in the microblx docs before continuing. In a nutshell, prior to the
introduction of the composition extensions, usc descriptions were
structured as flat lists of *blocks*, *configurations* and
*connections*. These models can be validated and launched using the
`ubx_launch` command line tool. Unsurprisingly, for complex systems
these flat descriptions can grow fairly large (i.e. several hundreds
of lines) and unwieldy. Worse than that, creating and maintaining
different applications variants requires duplicating large portions of
usc files, thereby making maintenance cumbersome.

To address this, the usc DSL was extended in the context of the COCORF
RobMoSys ITP project to support hierarchical composition of usc
files. The initial requirements for these are elaborated in
[this](https://github.com/kmarkus/microblx/blob/master/docs/dev/001-blockdiagram-composition.md)
document. This tutorial introduces the new mechanisms by step-by-step
building and testing of a cascaded controller application through
composition of layers of usc files.

### Installation

To simplify the installation process, this tutorial provides a
Dockerfile from which the tutorial environment can be built. To do so,
just install docker, check out this repository and start the build as
follows:


```sh
$ git clone gitlab.com:kmarkus/microblx-composition-tutorial.git
$ cd microblx-composition-tutorial/
$ docker build -t cocorf-demo:latest docker/
```

Now get yourself a cup of coffee. This will take a while. Once
completed, make sure to allow local X11 connections and test if they
work (This is required later for running the robot visualization
within the container):

```sh
$ xhost +local:
$ docker run -it --rm -e "DISPLAY=$DISPLAY" -v /tmp/.X11-unix:/tmp/.X11-unix cocorf-demo:latest xeyes
```

If everything went fine, you should have the `xeyes` app pop up.

Running the container with the tutorial environment can be done as
follows:

```sh
$ docker run -it --name=cocorf-demo --rm -e "DISPLAY=$DISPLAY" -v /tmp/.X11-unix:/tmp/.X11-unix cocorf-demo:latest bash
68efbf8f5d1c:~/src/git/microblx-composition-tutorial/usc#
```

For interacting with the system and for checking the debug
information, we will need additional shells in the container. This can
be done as follows:

```sh
$ docker exec -it cocorf-demo bash
```

It is recommended to always run `ubx-log` in a separate window in
order to see the state of the system and the results of interactions.

If the main terminal is closed (the one launched with `docker run`,
the container will be removed and all secondary shells closed too.


The Composable Cascaded Controller Application
----------------------------------------------

The objective of this tutorial is to build a reusable, cascaded
position controller and shows how it can be reused in different
scenarios. The following figure gives an overview of the different
compositions involved. The meaning of a composition being shown above
another one is that it *extends* or *is based on* the lower
one. Please note that the shown layering is not rigid and only the
example chosen here. Depending on the use-case, different
re-compositions can very well make sense.

![A possible layering of composition](figs/cascaded-ctrl-layers.svg)

### velctrl.usc: velocity control

Starting bottom up, [velctrl.usc](usc/velctrl.usc) defines a minimal
but complete velocity controller consisting of a few blocks, default
configurations and connections. The following is a graphical
representation of the file:

![velctrl composition](figs/velctrl.svg)

The model consists of the following four blocks:

1. `velctrl`: the actual velocity controller, which uses the standard
   microblx `ubx/pid` block.
2. `arm`: the robot arm. This is a dummy, ideal, manipulator block
   (`mc/manipulator`) that feeds through the commanded quantity. Its
   main purpose is to specify a standard manipulator interface and to
   serve as a placeholder for real manipulators.
3. `ctrl_mode`: a constant block that provides the desired control
   mode (in our case `velocity` to the `arm`. Instead of connecting a
   constant block to the port, the control mode could simply be
   specified as configuration. However, the former has the advantage
   that parent compositions can easily introduce support for
   additional, run-time switchable control modes simply by adding
   additional constants and enabling or disabling these.
4. `trig`: a `ubx/trig` trigger block. This block is configured with a
   schedule describing how to trigger the other blocks. This is shown
   as dotted, green lines above.
   
In the `connections` section of the usc file the connections for the
control loop are defined (solid green lines).

Please note the `node_configurations` section, which defines a global
parameter `num_joints`. This parameter is assigned to all block
configurations that require to be parameterized with the number of
joints such as the controllers by using the `&num_joints` syntax. We
will override that later when using the composition for a robot with a
different number of joints.

This `usc` is a complete, valid model and *can* be launched with the
following command

```sh
$ ubx-launch -c velctrl.usc
```

though the result is not very useful; if you're running `ubx-log` in a
separate window you will see how the system was instantiated and
configured as expected, but lacking an *active trigger* (like the
pthread based `ubx/ptrig`) to trigger our the `trig` block, the
application just sits there.

The quick and dirty way to add an active trigger would be to directly
edit `velctrl.usc` and change the type of `trig` to
`ubx/ptrig`. However, this approach would be undesirable since it
would severely reduce composabilty by polluting the generic model with
configurations (i.e. priority, scheduling policy etc.) that are both
highly platform- and application specific. The *composable* solution
to this is to specify the trigger in a separate usc file. The two
`usc` files [ptrig_nrt](usc/ptrig_nrt.usc) and
[ptrig_rt](usc/ptrig_rt.usc) specify a non-real-time and a real-time
trigger block (`ubx/ptrig`) which is configured to trigger a block
called `trig`. Such `ptrig_*` usc models can conveniently be merged
into the primary model by specifying them as an additional, comma
separated usc file parameter to `ubx-launch` as shown below:


```sh
$ ubx-launch -c velctrl.usc,ptrig_nrt.usc
```

if you check the output of `ubx-log`, you will see a lot of messages
like:

```
[31397.980967] velctrl ERROR: ENODATA: no data on port des
```

which are output by the velocity controller who is complaining about
being triggered without data being available on it *desired value*
port. Thus, to properly run the `velctrl` composition, an input to the
controller is needed. As this is an example of a cascaded controller,
this input will be the output of the "outer" controller, i.e. here of
a position controller described in [posctrl.usc](usc/posctrl.usc).

### posctrl.usc: the outer position controller

As can be seen in its graphical description, the
[posctrl.usc](usc/posctrl.usc) model is much simpler than the velocity
controller. This is owed to the fact that is merely extends
(highlighted in orange) the velocity controller with the position
control loop:

![posctrl composition](figs/posctrl.svg)

In contrast to previous example, where the `ptrig_nrt` model is
*merged* at launch-time on the command line, the position control
composition *includes* the velocity controller as a sub-composition
*inline*:

```Lua
subsystems = {
	bd.load("velctrl.usc")
}

```

This is not mandatory in any way but a design choice based on the
observation that this specific position controller composition can not
exist without the velocity controlled composition it is based on,
whereas in contrast the `ptrig_nrt` can be used with any composition
with a `trig` block.

Secondly, note how the composition sets the configuration of the
`trig` block in order to add the `posctrl` controller to the list of
triggered blocks. But since there are now two configurations for the
same block, how does `ubx-launch` know which one to choose? The answer
lies in the *conflict resolution policy*, which in turn depends on the
composition mechanism used:

- for *inline* compositions (using the `subsystems` keyword), *parent*
  compositions take precedence over (i.e. override) definitions in
  *child* compositions.
  
- the 2nd..Nth compositions passed to `ubx-launch` using the `-c` flag
  are merged into the first one starting from left to right, hence in
  case of conflicts, the definition the file specified last takes
  precedence.
  
The motivation for this policy is the same as for hierarchical
statecharts: it ensures that "small" changes (in the sense of deeply
nested in the hierarchical structure) can't have global impact.

While launchable, the position controller will, akin to the velocity
controller above, complain about the lack of an input to the
controller. This is tackled in the next composition.

### posctrl-test.usc: the test bench

Having defined the basic cascaded controller, it is now time to run it
for the first time. To that end we define a "test-harness" composition
[posctrl-test.usc](usc/posctrl-test.usc) which prepares the
environment for testing the controller:

1. create a test input signal for the controller (we use a sine here)
2. enable profiling of block execution time (config `trig.tstats_mode=2`)
3. export a number of signals for external access via mqueues.

Here's the graphical representation 

![posctrl-test composition](figs/posctrl-test.svg)

Now we have something to run and test:

```sh
$ ubx-launch -c posctrl-test.usc,ptrig_nrt.usc
```

in a second shell you can now use `ubx-mq` to inspect the different
signals:

```sh
$ ubx-mq list
   mq id            type name  array len  type hash
1  arm.jnt_pos_msr  double     6          e8cd7da078a86726031ad64f35f5a6c0
2  velctrl.out      double     6          e8cd7da078a86726031ad64f35f5a6c0
3  posctrl.out      double     6          e8cd7da078a86726031ad64f35f5a6c0
4  sin.y            double     6          e8cd7da078a86726031ad64f35f5a6c0
5  ramp.out         double     6          e8cd7da078a86726031ad64f35f5a6c0

$ ubx-mq read velctrl.out
{0.17817870116745,0.17817870116745,0.17817870116745,0.17817870116745,0.17817870116745,0.17817870116745}
{0.17363742731024,0.17363742731024,0.17363742731024,0.17363742731024,0.17363742731024,0.17363742731024}
{0.1673612256763,0.1673612256763,0.1673612256763,0.1673612256763,0.1673612256763,0.1673612256763}
{0.15941280599772,0.15941280599772,0.15941280599772,0.15941280599772,0.15941280599772,0.15941280599772}
{0.14987158625653,0.14987158625653,0.14987158625653,0.14987158625653,0.14987158625653,0.14987158625653}
...
```

When shutting down `ubx-lauch` (`Ctrl-C`), take note of the timing
statistics printed in the log:

```
[204713.687996] trig INFO: TSTAT: chain0,ramp: cnt 6962, min 1 us, max 52 us, avg 7 us
[204713.688003] trig INFO: TSTAT: chain0,sin: cnt 6962, min 1 us, max 706 us, avg 6 us
[204713.688007] trig INFO: TSTAT: chain0,arm: cnt 6962, min 1 us, max 212 us, avg 6 us
[204713.688037] trig INFO: TSTAT: chain0,velctrl: cnt 6962, min 1 us, max 113 us, avg 7 us
[204713.688039] trig INFO: TSTAT: chain0,posctrl: cnt 6962, min 0 us, max 31 us, avg 5 us
[204713.688043] trig INFO: TSTAT: chain0,#total#: cnt 6962, min 24 us, max 737 us, avg 44 us
```

As we are not running with real-time priorities, the large deviations
between `max` and `avg` are to be expected.

### kinova-arm.usc: switching to to a "real" robot

After successfully testing and validation of our controller, it is
time to switch to "real" robot. For this, we'll be using the bullet
based simulator block [sim2b](https://github.com/rosym-project/sim2b)
to simulate a kinova robot.

The following figures shows the changes brought in by the
[kinova-arm.usc](usc/kinova-arm.usc).

![kinova-arm composition](figs/kinova-arm.svg)

The composition adapts `posctrl.usc` for use with the kinova robot
manipulator. Apart from overriding the `arm` block type and adapting
it's configuration to the changed manipulator interface, an important
modification not shown in the graphical description is the following:

```Lua
node_configurations = {
	num_joints = { type = "long", config = 7 }
}
```

This change to the `num_joints` global configuration essentially
switches the controller from 6 to 7 DOF. Therefore, it is also
necessary to adapt the relevant controller gains to the new dimension.

The resulting composition is again launchable (`ubx-launch -c
kinova-arm.usc,ptrig_nrt.usc`) and permits controlling the robot by
writing joint space set-points and reading the robots position using
`ubx-mq`. You can give it a try by using the following two commands
(in separate terminals):

- `ubx-mq read ubx-mq read arm.jnt_pos_msr`
- `ubx-mq write posctrl.des '{1,1,1,1,1,1}' -r 0.001`

Of course, picturing robot position based on joint angles values is
not everyone's grass to mow, thus our last composition layer adds
basic visualization.

### kinova-viz.usc: adding visualization

To visualize the robot state, two blocks are required: one for the
forward position kinematics of the kinova and one for the actual
visualization. 

Thus [kinova-viz.usc](usc/kinova-viz.usc) contains no big surprises
anymore. It adds and connects the
[vis2b](https://github.com/rosym-project/vis2b) and
[kinova_fpk](https://gitlab.com/kmarkus/kinova_fpk) blocks, connects
then and updates `trig`s schedule. Here's the graphical view:

![kinova-viz composition](figs/kinova-viz.svg)

Similar to the previous ones, this composition can be run with:

```bash
$ ubx-launch -c kinova-viz.usc,ptrig_nrt.usc
```

This time, a graphical window showing the robot will pop-up.

Just as in the previous example, joint set-points can  be commended
using `ubx-mq`. For instance, try:

```sh
$ ubx-mq write posctrl.des '{1,1,1,1,1,1}' -r 0.001
$ ubx-mq write posctrl.des '{-1,-1,-1,-1,-1,-1}' -r 0.001
```

If everything went well, the robot should start moving.

### Summary

This section summarizes the different compositions used in the
previous sections.

| Command                                        | Resulting composition                                                         |
|------------------------------------------------|-------------------------------------------------------------------------------|
| `ubx-launch -c velctrl.usc,ptrig_nrt.usc`      | velocity controller with dummy manipulator                                    |
| `ubx-launch -c posctrl.usc,ptrig_nrt.usc`      | position controller with dummy manipulator                                    |
| `ubx-launch -c posctrl-test.usc,ptrig_nrt.usc` | the above with test input and exported ports                                  |
| `ubx-launch -c kinova-arm.usc,ptrig_nrt.usc`   | position controller but with a simulated arm instead of the dummy manipulator |
| `ubx-launch -c kinova-viz.usc,ptrig_nrt.usc`   | like above, but with visualization                                            |


Limitations
-----------

At this point is not yet possible to launch all possible, sensible
combinations of compositions. For instance, it would be reasonable to
compose the test input signals and signal exports from
`posctrl-test.usc` with the simulated and visualized robot from
`kinova-viz.usc` as follows:

```sh
$ ubx-launch -c posctrl-test.usc,kinova-viz.usc,ptrig_nrt.usc
```

However the resulting composition would not work as expected. The
reason is simple: the `trig` schedule provided by `kinova-viz` does
not trigger the additional `ramp` and `sin` blocks from
`posctrl-test.usc`. Fortunately, there is both a workaround and a
solution in sight. The workaround would be to define a new composition
which includes both `kinova-viz` and `posctrl-test.usc` and applies a
schedule that triggers all blocks. The solution is to introduce
support for automatic computation of schedules, which would free the
user from this aspect of composition. This feature has been foreseen
on the project roadmap.

Related work
------------

A similar example but using the UR5 and
[meshcat](https://github.com/rdeits/meshcat) can be found
[here](https://github.com/mfrigerio17/ublx-ur5_sim)

Acknowledgment
---------------

- The Kinova simulation and visualization blocks were developed and
  generously shared by the [VeriComp](https://robmosys.eu/vericomp/)
  sister ITP of COCORF.
  
- The COCORF ITP coaches Enea Scioni, Marco Frigerio and Herman
  Bruyninckx and have contributed many valuable suggestions that made
  the DSL and this demo itself.

- Development of the microblx composition extensions was supported by
  the European H2020 project RobMoSys via the COCORF (Component
  Composition for Real-time Function blocks) Integrated Technical
  Project.
