# Create a microblx sample image
FROM ubuntu:20.04

ENV SRC_PATH=/root/src/git/

CMD ["echo","installing apt packages"]
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y automake make libtool g++ pkg-config git-core luajit libluajit-5.1-dev lua-unit lua-filesystem uthash-dev cppcheck software-properties-common cmake libopenscenegraph-dev python3-dev x11-apps

# silence git warnings
RUN git config --global advice.detachedHead false
RUN git config --global user.name 'cocorf-demo'
RUN git config --global user.email 'cocorf-demo@localhost'

RUN mkdir -p $SRC_PATH

CMD ["echo","installing uutils"]
WORKDIR $SRC_PATH
RUN git clone -b v1.1.0 --depth=1 https://github.com/kmarkus/uutils
WORKDIR $SRC_PATH/uutils
RUN make install

CMD ["echo","installing bullet3"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 -b 3.05 https://github.com/bulletphysics/bullet3.git
WORKDIR $SRC_PATH/bullet3
RUN ./build_cmake_pybullet_double.sh
WORKDIR $SRC_PATH/bullet3/build_cmake
RUN make install

CMD ["echo","installing liblfds"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 https://github.com/liblfds/liblfds6.1.1
RUN git clone --depth=1 -b v0.9.1-rc2 https://github.com/kmarkus/microblx.git

WORKDIR $SRC_PATH/liblfds6.1.1
RUN git am ~/src/git/microblx/liblfds/*.patch
RUN ./bootstrap
RUN ./configure
RUN make -j3
RUN make install

CMD ["echo","installing microblx"]
WORKDIR $SRC_PATH/microblx
RUN ./bootstrap
RUN ./configure
RUN make -j3
RUN make install

CMD ["echo","installing microblx-motion-control"]
WORKDIR $SRC_PATH
RUN git clone -b cocorf https://github.com/kmarkus/microblx-motion-control
WORKDIR $SRC_PATH/microblx-motion-control
RUN mkdir build
WORKDIR $SRC_PATH/microblx-motion-control/build
RUN cmake ..
RUN make -j3
RUN make install

CMD ["echo","installing rosym/dyn2b"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 https://github.com/rosym-project/dyn2b.git
WORKDIR $SRC_PATH/dyn2b
RUN mkdir build
WORKDIR $SRC_PATH/dyn2b/build
RUN cmake ..
RUN make -j3
RUN make install

CMD ["echo","installing rosym/sim2b"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 -b use-pkgconfig-cflags https://github.com/kmarkus/sim2b.git
WORKDIR $SRC_PATH/sim2b
RUN mkdir build
WORKDIR $SRC_PATH/sim2b/build
RUN cmake -DENABLE_BULLET=1 -DENABLE_UBX=1 ..
RUN make -j3
RUN make install

CMD ["echo","installing kinova meshes"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 -b v2.2.2 https://github.com/Kinovarobotics/ros_kortex.git
COPY files/0001-GEN3_URDF_V12.urdf-minor-adjustments-for-COCORF-demo.patch /tmp/
WORKDIR $SRC_PATH/ros_kortex
RUN git am --whitespace=fix /tmp/0001-GEN3_URDF_V12.urdf-minor-adjustments-for-COCORF-demo.patch

CMD ["echo","installing rosym/viz2b"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 -b mk https://github.com/kmarkus/vis2b.git
WORKDIR $SRC_PATH/vis2b
RUN mkdir build
WORKDIR $SRC_PATH/vis2b/build
RUN cmake -DENABLE_UBX=1 ..
RUN make -j3
RUN make install

CMD ["echo","installing rosym/ctrl2b"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 https://github.com/rosym-project/ctrl2b.git
WORKDIR $SRC_PATH/ctrl2b
RUN mkdir build
WORKDIR $SRC_PATH/ctrl2b/build
RUN cmake ..
RUN make -j3
RUN make install

CMD ["echo","installing kinova_fpk"]
WORKDIR $SRC_PATH
RUN git clone --depth=1 https://gitlab.com/kmarkus/kinova_fpk.git
WORKDIR $SRC_PATH/kinova_fpk
RUN mkdir build
WORKDIR $SRC_PATH/kinova_fpk/build
RUN cmake ..
RUN make -j3
RUN make install

RUN ldconfig

CMD ["echo","installing microblx composition demo"]
WORKDIR $SRC_PATH
RUN git clone https://gitlab.com/kmarkus/microblx-composition-tutorial.git
WORKDIR $SRC_PATH/microblx-composition-tutorial/usc/

CMD ["echo","cocorf composition tutorial image created"]
